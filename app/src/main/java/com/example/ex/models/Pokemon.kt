package com.example.ex.models

data class Pokemon(
    var pokemonId: Int,
    var name: String,
    var thumbURL: String,
)