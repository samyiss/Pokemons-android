package com.example.ex

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ex.models.Pokemon
import com.squareup.picasso.Picasso


class PokemonRecyclerViewAdapter(private val listePokemon: MutableList<Pokemon>) :
    RecyclerView.Adapter<PokemonRecyclerViewAdapter.PokemonViewHolder>() {

    class PokemonViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.pokemons_item, parent, false) as View
        return PokemonViewHolder(view)
    }

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        holder.view.findViewById<TextView>(R.id.NomPok).text =
            this.listePokemon[position].name
        val imgAlbum = holder.view.findViewById<ImageView>(R.id.pokImage)
        Picasso.get().load(this.listePokemon[position].thumbURL).into(imgAlbum)

    }

    override fun getItemCount() = this.listePokemon.size
}