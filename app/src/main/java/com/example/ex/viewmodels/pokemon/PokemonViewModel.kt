package com.example.ex.viewmodels.pokemon

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.ex.models.Pokemon
import com.example.ex.repositories.PokemonRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Suppress("RedundantVisibilityModifier")
class PokemonViewModel(application: Application) : AndroidViewModel(application) {
    public var pokemons: MutableLiveData<MutableList<Pokemon>> = MutableLiveData(mutableListOf())

    init {
        viewModelScope.launch(Dispatchers.IO) {
            val pokemonRepository = PokemonRepository(getApplication())
            pokemonRepository.getPokemons(pokemons)
        }
    }
}