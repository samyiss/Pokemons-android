package com.example.ex

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ex.viewmodels.pokemon.PokemonViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var rvListePokemon: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.setContentView(R.layout.activity_main)

        var gridLayoutManager = GridLayoutManager(applicationContext,2)

        this.rvListePokemon = this.findViewById(R.id.rvPokemons)

        val pokemonViewModel = ViewModelProvider(this).get(PokemonViewModel::class.java)

        pokemonViewModel.pokemons.observe(this) {
            this.rvListePokemon.adapter = PokemonRecyclerViewAdapter(it)
        }

        rvListePokemon.layoutManager = gridLayoutManager
    }
}