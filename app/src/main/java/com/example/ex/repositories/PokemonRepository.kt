package com.example.ex.repositories

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.ex.models.Pokemon
import com.google.gson.Gson

class PokemonRepository(private val application: Application) {
    fun getPokemons(pokemons: MutableLiveData<MutableList<Pokemon>>) {
        val queue = Volley.newRequestQueue(application)
        val url = "https://pokemonsapi.herokuapp.com/pokemons"

        val r = StringRequest(
            Request.Method.GET,
            url,
            {
                val arrayPokemons = Gson().fromJson(it, Array<Pokemon>::class.java)
                pokemons.value = arrayPokemons.toMutableList()
            },
            {
                println("ERREUR: /api/pokemons")
            })

        queue.add(r)
    }
}